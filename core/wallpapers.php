<?php if ($root=="") exit; 

# Initiate page
echo '<div class="container">'."\n";

# Include the webcomic sources 
include($file_root.'core/mod-menu-lang.php');

  echo '  <article class="col sml-12 sml-text-center">'."\n";
  echo '  <h2>'._("Wallpapers").'</h2>'."\n";

  $content = "wallpapers";

  if(is_dir($sources.'/0ther/'.$content.'/low-res/')) {
    $all_thumbnails = glob($sources.'/0ther/'.$content.'/low-res/*.jpg');
    rsort($all_thumbnails);
    foreach ($all_thumbnails as $thumb) {
      $thumb_filename = basename($thumb);
      $thumb_fullpath = dirname($thumb);
      $thumb_option_link = str_replace('.jpg', '', $thumb_filename);
      $cover_description = $thumb_option_link; #TODO better title/alt
      echo '      <figure class="thumbnail col sml-12 med-6 lrg-4">'."\n";
      echo '        <a href="'.$root.'/'.$sources.'/0ther/'.$content.'/hi-res/'.$thumb_filename.'">'."\n";
      echo '          ';
        # Generated: all 400px height (and 900px bounding box for width). Good compromise to make ~200px thumb for large monitor, and big 400px thumb for phones.
        _img($sources.'/0ther/'.$content.'/low-res/'.$thumb_filename, $cover_description, 900, 400, 82);
      echo ''."\n";
      echo '        </a>'."\n";
      echo '      </figure>'."\n";
    }
  }

echo '  </article>'."\n";
echo '</div>'."\n";
echo ''."\n";
?>
