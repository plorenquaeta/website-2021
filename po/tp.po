# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-08 20:33-0700\n"
"PO-Revision-Date: 2021-08-30 19:15+0000\n"
"Last-Translator: Ret Samys <retsamys@gmx.net>\n"
"Language-Team: tp (generated) <https://weblate.framasoft.org/projects/"
"peppercarrot/website/tp/>\n"
"Language: tp\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.6.2\n"

#: core/404.php:7
msgid "Error 404: Page not found."
msgstr "pakala nanpa 404: lipu ni li lon ala."

#: core/about.php:15 core/tos.php:11
msgid ""
"Photo of the author, David Revoy with his cat named Noutti while drawing the "
"first episodes of Pepper&Carrot."
msgstr ""
"sitelen ni la, jan musi David Revoy li lon pona pi soweli ona Noutti, li "
"sitelen e lipu nanpa wan pi janPepa&soweli Kawa."

#: core/about.php:17
msgid "The author"
msgstr "jan musi"

#: core/about.php:19
msgid ""
"Hi, my name is David Revoy and I'm a French artist born in 1981. I'm self-"
"taught and passionate about drawing, painting, cats, computers, Gnu/Linux "
"open-source culture, Internet, old school RPG video-games, old mangas and "
"anime, traditional art, Japanese culture, fantasy…"
msgstr ""
"toki, mi jan David Revoy. mi jan musi tan ma Kanse. mi lon tan tenpo sike "
"1981. mi kama sona tan mi. sitelen linja en sitelen kule en soweli suwi en "
"ilo sona en nasin Open-source Gnu/Linux en musi pi tawa kulupu pi tenpo pini "
"en sitelen musi pi ma Nijon pi tenpo pini en sitelen tawa pi ma Nijon pi "
"tenpo pini en musi sitelen pi ilo sona ala en nasin Nijon en toki musi pi "
"lon ante li suli tawa mi…"

#: core/about.php:20
msgid ""
"After more than 10 years of freelance in digital painting, teaching, concept-"
"art, illustrating and art-direction, I decided to start my own project. I "
"finally found a way to mix all my passions together, the result is "
"Pepper&Carrot."
msgstr ""
"tenpo sike pini luka luka la, pali mi li sitelen kule pi ilo sona, li pana "
"sona, li sitelen pi musi esun, li sitelen pi poka toki, li lawa pi kulupu "
"musi. pali ni li pini la, mi wile pali e pali pi mi taso. tenpo ni la, mi "
"kama wan e wile mute ni mi, e pali mute ni mi. tan ni la, jan Pepa&soweli "
"Kawa li lon."

#: core/about.php:21
msgid ""
"I'm working on this project since May 2014, and I received the help of many "
"contributors and supporters on the way. "
msgstr ""
"mi open e pali ni lon tenpo mun nanpa luka lon tenpo sike nanpa 2015. jan "
"pona pali mute en jan pona mute pi pana pona li kama, li pana e pona tawa "
"pali mi. "

#: core/about.php:22
#, php-format
msgid ""
"You'll find the full list of credits on the <a href=\"%s\">License menu</a>."
msgstr ""
"sina ken lukin e nimi ale pi jan pona lon lipu pi <a href=\"%s\">nasin lawa</"
"a>."

#: core/about.php:24
msgid "My blog: "
msgstr "toki mi: "

#: core/about.php:25
msgid "My email: "
msgstr "nimi mi pi sitelen ilo: "

#: core/about.php:29
msgid "The philosophy of Pepper&Carrot"
msgstr "nasin pi jan Pepa&soweli Kawa"

#: core/about.php:31
msgid "Supported by patrons"
msgstr "jan pona li pana pona"

#: core/about.php:33
msgid ""
"Pepper&Carrot project is only funded by its patrons, from all around the "
"world. Each patron sends a little money for each new episode published and "
"gets a credit at the end of the new episode. Thanks to this system, "
"Pepper&Carrot can stay independent and never have to resort to advertising "
"or any marketing pollution."
msgstr ""
"pali jan Pepa&soweli Kawa li kama taso tan jan pona pi pana mani. jan ni li "
"kama tan ma mute tan ma ale. jan wan ni li pana e mani lili tawa lipu toki "
"wan. tan ni la, nimi ona li sitelen lon lipu toki sin. nasin ni la, jan "
"Pepa&soweli Kawa li ken awen kepeken wile ona taso kepeken nasin ona taso. "
"sitelen esun ike en toki esun ike li wile ala."

#: core/about.php:35
msgid "Pepper and Carrot receiving money from the audience."
msgstr "jan Pepa en soweli Kawa li kama jo e mani tan jan lukin."

#: core/about.php:37
msgid "100&#37; free(libre), forever, no paywall"
msgstr ""
"100&#37; jan ale li ken jo, kepeken nasin Free(libre), lon tenpo ale, "
"kepeken mani ala"

#: core/about.php:39
msgid ""
"All the content I produce about Pepper&Carrot is on this website or on my "
"blog, free(libre) and available to everyone. I respect all of you equally: "
"with or without money. All the goodies I make for my patrons are also posted "
"here. Pepper&Carrot will never ask you to pay anything or to get a "
"subscription to get access to new content."
msgstr ""
"jan Pepa&soweli Kawa la, lipu ni la, jan ale li ken jo e pali ale mi. jan "
"Pepa&soweli Kawa la, lipu ni la, pali ale mi li jo pi jan ale, li nasin "
"Free(libre). sina pana ala pana e mani - ken ni tu la, sina ale li pona sama "
"tawa mi. pali mi namako li tawa jan pona pi pana pona la, pali mi namako li "
"lon lipu ni kin. tenpo ala la, jan Pepa&amp;soweli Kawa li alasa sina tan "
"mani sina. tenpo ala la, sina ken jo e pali sin tan mani taso."

#: core/about.php:41
msgid "Carrot, locked behind a paywall."
msgstr "soweli Kawa li ken ala tawa tan sinpin pi nasin mani taso."

#: core/about.php:44
msgid "Open-source and permissive"
msgstr "nasin Open-source en ken mute"

#: core/about.php:46
msgid "Creative Commons Attribution 4.0 International license."
msgstr "nasin lawa Creative Commons Attribution 4.0 International."

#: core/about.php:46
msgid ""
"I want to give people the right to share, use, build and even make money "
"upon the work I've created. All pages, artworks and content were made with "
"Free(Libre) Open-Sources Software on GNU/Linux, and all sources are on this "
"website (Sources and License buttons). Commercial usage, translations, fan-"
"art, prints, movies, video-games, sharing, and reposts are encouraged. You "
"just need to give appropriate credit to the authors (artists, correctors, "
"translators involved in the artwork you want to use), provide a link to the "
"license, and indicate if changes were made. You may do so in any reasonable "
"manner, but not in any way that suggests the authors endorse you or your "
"use. More information can be read about it here: "
msgstr ""
"mi wile pana e ken jo, e ken pana, e ken pali, e ken esun, e ken mani tawa "
"jan ale kepeken pali mi. lipu ale en sitelen ale en musi ale en pali ale li "
"kepeken taso ilo Free(Libre) pi nasin Open-Source lon ilo GNU/Linux. pali "
"mama ale li lon lipu ni. (o lukin e lipu pi pali mama e lipu pi nasin lawa.) "
"mi wile e ni: sina ante toki e pali mi, li pali e sitelen sama pali mi, li "
"sitelen ilo e sitelen mi, li tawa e sitelen mi, li pali e musi pi pali mi, "
"li pana e pali mi tawa jan ante, li esun e pali mi. pali ni taso li wile "
"taso: o nimi e jan musi (sina wile kepeken musi la, jan musi li jan musi "
"sitelen, li jan pi ante toki, li jan pi pona pali.), o pana e nasin ilo tawa "
"nasin lawa, o toki e ante sina. sina ken pali mute. sina o toki ala e ni, o "
"pali ala sama ni: sina anu pali sina li pona tawa jan pi mama pali anu jan "
"musi. sina wile sona la, o lukin e ni: "

#: core/about.php:48
msgid "Example of derivatives possible."
msgstr "sama sitelen ni la ante mute li ken."

#: core/about.php:50
msgid "Quality entertainment for everyone, everywhere"
msgstr "musi musi pona tawa jan ale, lon ma ale"

#: core/about.php:52
msgid ""
"Pepper&Carrot is a comedy/humor webcomic suited for everyone, every age. No "
"mature content, no violence. Free(libre) and open-source, Pepper&Carrot is a "
"proud example of how cool free-culture can be. I focus a lot on quality, "
"because free(libre) and open-source doesn't mean bad or amateur. Quite the "
"contrary."
msgstr ""
"jan Pepa&soweli Kawa li sitelen pi toki musi, li musi pi pilin pona, li tawa "
"jan ale, li tawa jan lili, li tawa jan suli. pali pi utala ike li lon ala. "
"pali ni li lon ala: sitelen pi musi ala li tawa jan suli. pali li kepeken "
"nasin Free(libre), li kepeken nasin Open-source. jan Pepa&soweli Kawa li ken "
"suli li pali suli li pona mute tawa kulupu pu jo pi jan ala, tawa nasin "
"kulupu pi mani ala. mi wile e ni: pali mi en sitelen mi li pona lukin mute "
"mute tan ni: nasin Free(libre) en nasin Open-source li ike lukin ala. ala "
"ala ala."

#: core/about.php:54
msgid "Comic pages around the world."
msgstr "lipu musi li tawa ma ale."

#: core/about.php:56
msgid "Let's change comic industry!"
msgstr "mi ale li ken ante e nasin esun pi sitelen pi toki musi!!"

#: core/about.php:58
msgid ""
"Without intermediary between artist and audience you pay less and I benefit "
"more. You support me directly. No publisher, distributor, marketing team or "
"fashion police can force me to change Pepper&Carrot to fit their vision of "
"'the market'. Why couldn't a single success 'snowball' to a whole industry "
"in crisis? We'll see…"
msgstr ""
"sina pana e mani tawa jan ante la jan ante li pana e mani tawa jan pi mama "
"musi la, sina pana e mani mute. tan ni la, mi jo e mani lili taso. taso sina "
"pana e mani tawa jan pi mama musi taso la, sina pana e mani lili taso. tan "
"ni la, mi jo e mani mute. jan esun ante li weka la, pona li mute tawa mi. "
"sina pana e mani tawa mi la, sina pana e pona tawa mi taso. nasin jan "
"Pepa&amp;soweli Kawa li ante ala tan jan pi esun lipu, tan jan pi pana esun "
"lipu, tan jan pi toki esun, tan jan pi toki utala pi nasin pona. nasin esun "
"en nasin jan Pepa&amp;soweli Kawa li nasin pi jan ni ala. nasini esun pi "
"lipu sitelen pi toki musi li kama ike, li weka e mani. pali lili ni li ken "
"ala ken ante e nasin esun suli, li ken ala ken pona e nasin esun suli? ni li "
"ken…"

#: core/about.php:60
msgid ""
"Diagram: on the left-hand side, Carrot is losing money with many middle-men. "
"On the right-hand side, the result is more balanced."
msgstr ""
"sitelen tu li lon: sitelen nanpa wan la, mani li weka lon soweli Kawa tan "
"jan esun pali mute. sitelen nanpa tu la, mani li awen sama tawa kulupu ale, "
"li kama lili ala lon soweli Kawa."

#: core/about.php:63 core/homepage.php:27 core/homepage.php:28
#: core/mod-header.php:148
msgid "Become a patron"
msgstr "o pana e mani"

#: core/artworks.php:24 core/mod-footer.php:89 core/mod-header.php:110
#: index.php:293 index.php:314
msgid "Artworks"
msgstr "sitelen ante"

#: core/artworks.php:29 index.php:323
msgid "Sketchbook"
msgstr "sitelen pi pali lili"

#: core/artworks.php:34 index.php:320
msgid "Misc"
msgstr "ijo ante"

#: core/artworks.php:38 core/contribute.php:34 core/files.php:168
msgid "Sources explorer"
msgstr "pali mama"

#: core/contribute.php:13
#, php-format
msgid ""
"Thanks to the <a href=\"%s\">Creative Commons license</a>, you can "
"contribute Pepper&Carrot in many ways."
msgstr ""
"<a href=\"%s\">nasin lawa Creative Commons</a> la, sina ken pana e pona tawa "
"jan Pepa&soweli Kawa kepeken nasin mute."

#: core/contribute.php:14
#, php-format
msgid ""
"Join the <a href=\"%s\">Pepper&Carrot chat room</a> to share your projects!"
msgstr ""
"o kama lon <a href=\"%s\">kulupu toki pi jan Pepa&soweli Kawa</a>, o pana e "
"pali sina tawa lukin!"

#: core/contribute.php:15
msgid ""
"(Note: English language is used accross our documentations, wiki and "
"channels.)"
msgstr "(o sona: lipu sona en sona ilo en toki pali la, mi kepeken toki Inli.)"

#: core/contribute.php:28
msgid "Your Derivations"
msgstr "ante pali sina"

#: core/contribute.php:29
msgid "Fan-art Gallery"
msgstr "pali pona pi jan ante"

#: core/contribute.php:30
msgid "Translation Documentation"
msgstr "sona ilo pi ante toki"

#: core/contribute.php:31
msgid "Beta-reading Forum"
msgstr "toki lon lukin pi lipu kama"

#: core/contribute.php:32
msgid "Wiki: Document the Universe"
msgstr "lipu sona: o pana e sona tan ma pali"

#: core/contribute.php:33
msgid "Cosplay"
msgstr "len"

#: core/contribute.php:35
msgid "Git repositories"
msgstr "toki ilo lon ilo Git"

#: core/fan-art.php:32
#, php-format
msgid "%d Fan-art"
msgid_plural "%d Fan-art"
msgstr[0] "pali pona %d pi jan ante"
msgstr[1] "pali pona %d pi jan ante"

#: core/fan-art.php:37 core/files.php:58
msgid "Fan comics"
msgstr "sitelen musi toki pi jan ante"

#. Fan Fiction main menu
#: core/fan-art.php:42 core/fan-art.php:101 core/files.php:59
msgid "Fan Fiction"
msgstr "toki musi pi jan ante"

#: core/fan-art.php:47
msgid "How to:"
msgstr "o pana kepeken nasin ni:"

#: core/fan-art.php:48
#, fuzzy, php-format
msgid ""
"Send me your artworks, comics and fiction using social-medias (tag me, my "
"social media links are written in the footer of this website) or send them "
"to me on <a href=\"%s\">the chat room</a> and I'll repost them here. "
msgstr ""
"o pana e pali sina pi sama pi pali mi, e sitelen musi toki sina, e toki musi "
"sina kepeken ilo toki kulupu anu lipu toki kulupu. (o sitelen tawa mi "
"kepeken nimi mi. mi pana e nimi ilo mi lon anpa pi lipu ni.) ante la, o pana "
"e ona lon %s<a href=\"%s\">kulupu toki</a>. ni la, mi ken pana e ona lon "
"lipu ni. "

#: core/fan-art.php:76
msgid "External history link to see all changes made to this page"
msgstr "o lukin e ante pali ale lon lipu tan ilo ante"

#: core/fan-art.php:78
msgid "View history"
msgstr "o lukin e ante pali ale"

#: core/fan-art.php:83
msgid "Edit this page with an external editor"
msgstr "o ante e lipu ni kepeken ilo lon lipu ante"

#: core/fan-art.php:85
msgid "Edit"
msgstr "o ante e ni"

#. Sidebar menu
#. ============
#. Dynamic services:
#: core/files.php:38
msgid "Episodes"
msgstr "lipu ale"

#: core/files.php:39
msgid "All Comic Panels"
msgstr "sitelen ale tan lipu ale"

#: core/goodies.php:22
msgid "Free Wallpapers"
msgstr "sitelen suli pi pona lukin kepeken mani ala"

#: core/goodies.php:23
msgid "Free Brushes and Resources"
msgstr "ilo sitelen kepeken mani ala"

#: core/goodies.php:24
msgid "Free Tutorials and Making-of"
msgstr "toki pi nasin pali kepeken mani ala"

#: core/goodies.php:25
msgid "Free Avatar Generators"
msgstr "sitelen sinpin pi ilo toki kepeken mani ala"

#: core/goodies.php:26
msgid "Free Install Guides for Linux and Tablets"
msgstr ""
"nasin pi kama pi ilo Linux en nasin pi kama pi ilo supa sitelen kepeken mani "
"ala"

#: core/goodies.php:27
msgid "Free Pepper&Carrot Fonts"
msgstr "ilo pi sitelen nimi tan jan Pepa&soweli Kawa kepeken mani ala"

#: core/homepage.php:22
msgid ""
"A free(libre) and open-source webcomic supported directly by its patrons to "
"change the comic book industry!"
msgstr ""
"sitelen ni pi toki musi li nasin jo pi jan mute, li nasin Free(libre), li "
"nasin Open-source, li tan mani tan jan pona mute. tan ni la, nasin esun pi "
"sitelen musi li ken kama ante!"

#: core/homepage.php:24 core/homepage.php:25
msgid "Read the webcomic for free"
msgstr "o lukin e lipu kepeken mani ala"

#: core/homepage.php:36
msgid "Latest episode"
msgstr "lipu sin"

#. In case the cover is not available in the current language, fallback to English.
#: core/homepage.php:55
#, php-format
msgid "Click to read episode %d."
msgstr "sina wile lukin e lipu nanpa %d la, o luka e ni."

#: core/homepage.php:69
#, php-format
msgid "Show %d episode"
msgid_plural "Show all %d episodes"
msgstr[0] "o lukin e lipu %d"
msgstr[1] "o lukin e lipu %d"

#: core/homepage.php:79
msgid "Recent blog-posts"
msgstr "toki mi sin"

#: core/homepage.php:137
msgid "(Note: in English only)"
msgstr "(o sona: ni li lon toki Inli taso)"

#: core/homepage.php:138
msgid "Go to the blog"
msgstr "o lukin e toki mi"

#: core/homepage.php:145
msgid "Support Pepper&Carrot on"
msgstr "o pana e mani tawa jan Pepa&soweli Kawa lon ilo ni:"

#: core/homepage.php:151
msgid "More options"
msgstr "ilo mani ante"

#: core/lib-credits.php:36
msgid "Art:"
msgstr "musi sitelen:"

#: core/lib-credits.php:41
msgid "Scenario:"
msgstr "toki:"

#: core/lib-credits.php:47
msgid "Script-doctor:"
msgstr "pona pi kon toki:"

#: core/lib-credits.php:53
msgid "Inspiration:"
msgstr "toki li sama toki ni:"

#: core/lib-credits.php:59
msgid "Beta-readers:"
msgstr "jan pi lukin pona:"

#: core/lib-credits.php:66
msgid "(original version)"
msgstr "(kepeken toki mama)"

#: core/lib-credits.php:69 core/lib-credits.php:168
msgid "Translation:"
msgstr "ante toki:"

#: core/lib-credits.php:75 core/lib-credits.php:174
msgid "Proofreading:"
msgstr "jan pi pona toki:"

#: core/lib-credits.php:81 core/lib-credits.php:180
msgid "Contribution:"
msgstr "pali ante:"

#: core/lib-credits.php:87
#, fuzzy
msgid "Notes:"
msgstr "o sona:"

#: core/lib-credits.php:123
msgid "published on"
msgstr "lon tenpo"

#: core/lib-credits.php:139
msgid "Creation:"
msgstr "mama:"

#: core/lib-credits.php:142
msgid "Lead Maintainer:"
msgstr "jan awen nanpa wan:"

#: core/lib-credits.php:145
msgid "Writers:"
msgstr "jan pi toki sitelen:"

#: core/lib-credits.php:148
msgid "Correctors:"
msgstr "jan pi pona pali:"

#: core/lib-credits.php:161
msgid "Website translation:"
msgstr "ante toki pi lipu ilo:"

#: core/lib-credits.php:195
msgid "Technical maintenance and scripting:"
msgstr "jan ni li awen e ilo, li pali e toki ilo:"

#: core/lib-credits.php:198
msgid "General maintenance of the database of SVGs:"
msgstr "sitelen SVG la, awen sona li awen tan jan ni:"

#: core/lib-credits.php:201
msgid "Website maintenance and new features:"
msgstr "jan ni li awen e lipu ilo, li pana e ijo sin tawa lipu ilo:"

#: core/lib-navigation.php:93
msgid "First"
msgstr "lipu open"

#: core/lib-navigation.php:94
msgid "Previous"
msgstr "lipu pini"

#: core/lib-navigation.php:95
msgid "Next"
msgstr "lipu kama"

#: core/lib-navigation.php:96
msgid "Last"
msgstr "lipu sin"

#: core/license.php:13 core/mod-webcomic-sources.php:62 core/viewer.php:142
msgid "License:"
msgstr "nasin lawa:"

#. A note here, because the Copyright © will probably rise eyebrows on a Free/Libre and Open-source Project.
#. It is advised to declare a 'copyright holder' before also declaring releasing as Creative Commons for some copyright code of some countries.
#. Note to translators: The %s placeholders are an opening and a closing link tag
#: core/license.php:18
#, php-format
msgid ""
"This work is licensed under a %sCreative Commons Attribution 4.0 "
"International license%s"
msgstr ""
"pali ni li kepeken nasin lawa %sCreative Commons Attribution 4.0 "
"International%s"

#: core/license.php:19
msgid "Copyright © David Revoy"
msgstr "© jan David Revoy li lawa e musi e pali"

#: core/license.php:21
msgid "Here is the total list of attributions for the selected language:"
msgstr ""
"kepeken toki pi wile sina la, ni li nimi pi jan pali ale, li nimi pi pali "
"ale, li nimi pi ilo ale:"

#: core/license.php:23
msgid "Full attribution:"
msgstr "nimi ale:"

#: core/license.php:26
msgid "Project management:"
msgstr "lawa pi kulupu pali:"

#: core/license.php:32
msgid "Hereva worldbuilding:"
msgstr "pali toki pi ma Elewa:"

#: core/license.php:36
msgid "Episodes:"
msgstr "lipu ale:"

#. Special thanks:
#. ------------------
#: core/license.php:49
msgid "Special thanks to:"
msgstr "pona suli li tan jan ni:"

#. to all translators
#. ------------------
#: core/license.php:52
msgid ""
"All patrons of Pepper&amp;Carrot! Your support made all of this possible."
msgstr ""
"jan pona ale pi pana mani pi jan Pepa&amp;soweli Kawa! ni ale li ken tan "
"sina."

#. to all translators
#. ------------------
#: core/license.php:56
msgid "All translators on Pepper&amp;Carrot for their contributions:"
msgstr "jan ale pi ante toki pi jan Pepa&amp;soweli Kawa tan pali ale ona:"

#. to the projects
#. ----------------
#: core/license.php:86
msgid ""
"<strong>The Framasoft team</strong> for hosting our oversized repositories "
"via their Gitlab instance Framagit."
msgstr ""
"<strong>kulupu Framasoft</strong> tan ni: pali mi li wile e suli mute a lon "
"ilo Framagit sama ilo Gitlab."

#: core/license.php:90
msgid ""
"<strong>The Libera.Chat staff and Matrix.org staff</strong> for our "
"#pepper&carrot community channel."
msgstr ""
"<strong>kulupu Libera.Chat en kulupu Matrix.org</strong> tan kulupu toki mi "
"#pepper&carrot."

#: core/license.php:94
msgid ""
"<strong>All Free/Libre and open-source software</strong> because "
"Pepper&Carrot episodes are created using 100&#37; Free/Libre software on a "
"GNU/Linux operating system. The main ones used in production being:"
msgstr ""
"<strong>ilo pi nasin jo pi jan ale en ilo Free/Libre en ilo Open-source</"
"strong> tan ni: lipu ale pi jan Pepa&soweli Kawa li kepeken ilo ni taso lon "
"ilo suli GNU/Linux. ilo ni li suli tawa pali:"

#: core/license.php:95
msgid "- <strong>Krita</strong> for artworks (krita.org)."
msgstr "- <strong>ilo Krita</strong> tan pali sitelen (krita.org)."

#: core/license.php:96
msgid ""
"- <strong>Inkscape</strong> for vector and speechbubbles (inkscape.org)."
msgstr ""
"- <strong>ilo Inkscape</strong> tan sitelen pi nasin linja, tan poki toki "
"kin (inkscape.org)."

#: core/license.php:97
msgid ""
"- <strong>Blender</strong> for artworks and video editing (blender.org)."
msgstr ""
"- <strong>ilo Blender</strong> tan pali sitelen, tan pali pi sitelen tawa "
"(blender.org)."

#: core/license.php:98
msgid "- <strong>Kdenlive</strong> for video editing (kdenlive.org)."
msgstr ""
"- <strong>ilo Kdenlive</strong> tan pali pi sitelen tawa (kdenlive.org)."

#: core/license.php:99
msgid "- <strong>Scribus</strong> for the book project (scribus.net)."
msgstr ""
"- <strong>ilo Scribus</strong> tan pali pi lipu suli kiwen (scribus.net)."

#: core/license.php:100
msgid "- <strong>Gmic</strong> for filters and effects (gmic.eu)."
msgstr ""
"- <strong>ilo Gmic</strong> tan ni: sitelen li ken kama ante namako (gmic."
"eu)."

#: core/license.php:101
msgid ""
"- <strong>ImageMagick</strong> & <strong>Bash</strong> for 90&#37; of "
"automation on the project."
msgstr ""
"- <strong>ilo ImageMagick</strong> & <strong>ilo Bash</strong> tan ni: pali "
"mute mute a li wile ala e pali tan jan, li wile e pali tan ilo taso (tawa "
"pali 90&#37;)."

#: core/license.php:107
msgid ""
"And finally to all developers who interacted on fixing Pepper&Carrot "
"specific bug-reports:"
msgstr ""
"pini la, pona li tan jan ni pi pali ilo: ona li pona e pakala ilo pi jan "
"Pepa&soweli Kawa taso:"

#: core/license.php:109
msgid "... and anyone I've missed."
msgstr "... mi weka e sona la, pona li tan jan ante kin a."

#: core/mod-footer.php:6
msgid "Follow Pepper&Carrot on:"
msgstr "sina ken lukin e jan Pepa&soweli Kawa lon ilo ni:"

#: core/mod-footer.php:25
msgid "Join community chat rooms:"
msgstr "o toki lon kulupu ni:"

#: core/mod-footer.php:27
msgid "IRC: #pepper&carrot on libera.chat"
msgstr "nasin IRC: nimi #pepper&carrot lon ilo libera.chat"

#: core/mod-footer.php:68 index.php:294 index.php:326
msgid "Wiki"
msgstr "lipu sona"

#: core/mod-footer.php:71
msgid "Making-of"
msgstr "nasin pali"

#: core/mod-footer.php:74
msgid "Brushes"
msgstr "ilo sitelen"

#: core/mod-footer.php:77 core/wallpapers.php:10 index.php:302 index.php:325
msgid "Wallpapers"
msgstr "sitelen suli pi pona lukin"

#: core/mod-footer.php:83 index.php:296
msgid "Homepage"
msgstr "lipu open"

#: core/mod-footer.php:86 core/mod-header.php:105 index.php:298
msgid "Webcomics"
msgstr "sitelen musi"

#: core/mod-footer.php:92 core/mod-header.php:115 index.php:299
msgid "Goodies"
msgstr "ijo pona"

#: core/mod-footer.php:95 core/mod-header.php:120 index.php:300
msgid "Contribute"
msgstr "pali sina"

#: core/mod-footer.php:98 core/mod-header.php:134 index.php:317
msgid "Shop"
msgstr "esun"

#: core/mod-footer.php:101 core/mod-header.php:138
msgid "Blog"
msgstr "toki mi"

#: core/mod-footer.php:104 core/mod-header.php:125 index.php:304
msgid "About"
msgstr "ni li seme"

#: core/mod-footer.php:107 core/mod-header.php:130 index.php:306
msgid "License"
msgstr "nasin lawa"

#: core/mod-footer.php:110 index.php:308
msgid "Terms of Services and Privacy"
msgstr "ilo li awen e sona seme tan sina?"

#: core/mod-footer.php:113
msgid "Code of Conduct"
msgstr "lipu ni en pali ni la, o kepeken nasin ni"

#. Prepare strings that will get reused in header:
#: core/mod-header.php:4
msgid "Pepper&amp;Carrot"
msgstr "jan Pepa&amp;soweli Kawa"

#: core/mod-header.php:7
msgid ""
"Official homepage of Pepper&amp;Carrot, a free(libre) and open-source "
"webcomic about Pepper, a young witch and her cat, Carrot. They live in a "
"fantasy universe of potions, magic, and creatures."
msgstr ""
"ni li lipu mama pi sitelen pi jan Pepa&amp;soweli Kawa. sitelen ni li nasin "
"jo pi jan ale, li nasin Free(libre), li nasin Open-source. toki pi sitelen "
"ni li toki e jan lili Pepa pi wawa nasa, e soweli ona Kawa. ona tu li lon ma "
"pi wawa nasa pi soweli sewi pi telo ken."

#. %1$s is the locale code, %2$s is the language name in English
#: core/mod-menu-lang.php:126
#, php-format
msgid "%1$s/%2$s translation (bookmarked)"
msgstr "%1$s/ante toki pi toki \"%2$s\" (awen)"

#. %1$s is the locale code, %2$s is the language name in English
#: core/mod-menu-lang.php:129
#, php-format
msgid "%1$s/%2$s translation"
msgstr "%1$s/ante toki pi toki \"%2$s\""

#: core/mod-menu-lang.php:145
#, php-format
msgid "%d language"
msgid_plural "all %d languages"
msgstr[0] "toki %d li lon"
msgstr[1] "toki %d li lon"

#. Menu 2: full list with percents:
#. --------------------------------
#. Define some translatable strings that will be reused.
#. This will save execution time on fetching the translations with gettext.
#. I am splitting off the website info in case we want to get a completion percentage
#. there in the future - this will save retranslation effort.
#. Placeholders: %1$s = language name, %2$s = locale code
#: core/mod-menu-lang.php:161
#, php-format
msgid "%1$s (%2$s): The translation is complete."
msgstr "%1$s (%2$s): ante pi toki ni li pini a."

#. Website translation is at 100%
#: core/mod-menu-lang.php:163
msgid "The website has been translated."
msgstr "ante toki pi lipu ilo li pini."

#. Website translation needs work
#: core/mod-menu-lang.php:165
msgid "The website is being translated."
msgstr "ante toki pi lipu ilo li open."

#. Website translation is below minimum completion
#: core/mod-menu-lang.php:167
msgid "The website has not been translated yet."
msgstr "ante toki pi lipu ilo li open ala."

#: core/mod-menu-lang.php:229
msgid "The star congratulates a 100&#37; complete translation."
msgstr "mun pona ni la, ale pi ante toki li pini a."

#. Combine website completion statement with comic percentage string to assemble tooltip
#. Placeholders: %1$s = language name, %2$s = locale code, %3$d = percentage (plural controller), %4$s = Sentence explaining website completion status. &#37; is the % symbol
#: core/mod-menu-lang.php:244
#, php-format
msgid "%1$s (%2$s): Comics %3$d&#37; translated. %4$s"
msgid_plural "%1$s (%2$s): Comics %3$d&#37; translated. %4$s"
msgstr[0] "toki \"%1$s\" (%2$s): lipu %3$d&#37; li kama ante toki. %4$s"
msgstr[1] "toki \"%1$s\" (%2$s): lipu %3$d&#37; li kama ante toki. %4$s"

#: core/mod-menu-lang.php:265
#, php-format
msgid "Save %s as favorite language"
msgstr "o awen e ni: toki \"%s\" li pona tawa sina"

#: core/mod-menu-lang.php:270
msgid "Add a translation"
msgstr "o pana e ante toki"

#: core/mod-webcomic-sources.php:30
msgid ""
"Comic pages of Pepper&Carrot comes from two sources: the illustration and "
"the text."
msgstr "lipu pi jan Pepa&soweli Kawa li tan nasin mama tu: sitelen en toki."

#: core/mod-webcomic-sources.php:31
msgid ""
"This page offers links to download them, but also to download ready to use "
"compiled rendering."
msgstr ""
"lipu ilo ni la, sina ken kama jo e ona tu, taso sina ken kama jo e lipu kin "
"pi kama wan."

#: core/mod-webcomic-sources.php:43
msgid "Cover of the episode"
msgstr "sitelen pi toki open"

#: core/mod-webcomic-sources.php:54
msgid "Git directory"
msgstr "lon ilo Git"

#: core/mod-webcomic-sources.php:55
msgid "Git history"
msgstr "ante ale lon ilo Git"

#: core/mod-webcomic-sources.php:64
msgid ""
"If you republish this episode, you need to provide the following information:"
msgstr ""
"sina wile pana e lipu ni lon lipu ante anu ilo ante la, o pana e sona ni:"

#: core/mod-webcomic-sources.php:65 core/viewer.php:145
msgid "Creative Commons Attribution 4.0 International license"
msgstr "nasin lawa Creative Commons Attribution 4.0 International"

#: core/mod-webcomic-sources.php:66
msgid "Attribution to:"
msgstr "o nimi e ni:"

#: core/mod-webcomic-sources.php:68
msgid "Credit for the universe of Pepper&Carrot, Hereva:"
msgstr "mama ale pi ma Elewa tan ma pi jan Pepa&soweli Kawa:"

#: core/mod-webcomic-sources.php:70
msgid ""
"Note: these credits are different depending the episode selected and the "
"language."
msgstr "o sona e ni: lipu en toki li ante la, nimi ni pi mama ale li ante."

#: core/mod-webcomic-sources.php:71 core/viewer.php:147
#, php-format
msgid ""
"More information and good practice for attribution can be found <a href=\"%s"
"\">on the documentation</a>."
msgstr ""
"sina wile sona e sona, e nasin pona pi pana nimi la, o lukin e <a href=\"%s"
"\">lipu pi sona nasin</a>."

#. Single-page
#: core/mod-webcomic-sources.php:76
msgid "Collage of all pages into a single image file"
msgstr "lipu ale lon lipu wan"

#: core/mod-webcomic-sources.php:92
msgid "<strong>Illustration sources</strong>: Krita (kra) pages."
msgstr "<strong>sitelen mama</strong>: lipu pi ilo Krita (kra)."

#: core/mod-webcomic-sources.php:105
msgid "<strong>Text sources:</strong> Inkscape (svg) pages."
msgstr "<strong>toki mama:</strong> lipu pi ilo Inkscape (svg)."

#: core/mod-webcomic-sources.php:122
msgid "Ready to use compiled renderings:"
msgstr "sitelen en toki li kama wan:"

#: core/mod-webcomic-sources.php:123
msgid "High quality and recommended for printing.<br/><br/>"
msgstr "sitelen suli ni li pona tawa ilo pi pana sitelen.<br/><br/>"

#. %d is the page number
#: core/mod-webcomic-sources.php:151 core/webcomic.php:185 core/xyz.php:59
#, fuzzy, php-format
#| msgid "Page"
msgid "Page %d"
msgstr "lipu lili nanpa"

#: core/mod-webcomic-sources.php:154
msgid "Credits"
msgstr "mama"

#: core/mod-webcomic-sources.php:157 core/webcomic.php:167 core/xyz.php:44
msgid "Header"
msgstr "nimi"

#: core/mod-webcomic-sources.php:160
msgid "Cover"
msgstr "sitelen pi toki open"

#. %s is the page number or page description (e.g. header, cover or credits)
#: core/mod-webcomic-sources.php:163
#, fuzzy, php-format
#| msgid "click to enlarge."
msgid "%s, click to enlarge."
msgstr "sitelen suli la, o luka."

#: core/mod-webcomic-sources.php:172
msgid "Illustration and text"
msgstr "sitelen en toki"

#: core/mod-webcomic-sources.php:173
msgid "Only illustration"
msgstr "sitelen taso"

#: core/mod-webcomic-sources.php:174
msgid "Only text"
msgstr "toki taso"

#: core/mod-webcomic-sources.php:187
msgid "Gif"
msgstr "sitelen Gif"

#: core/mod-webcomic-sources.php:188
msgid "click to enlarge."
msgstr "sitelen suli la, o luka."

#: core/mod-webcomic-sources.php:197
msgid "Animation"
msgstr "sitelen musi tawa"

#: core/mod-webcomic-sources.php:198
msgid "Static version for print"
msgstr "sitelen pi tawa ala, tawa ilo pi pana sitelen"

#: core/setup.php:14
msgid "Return to the previous page"
msgstr "o kama sin lon lipu lukin pi tenpo pini"

#: core/setup.php:16
msgid "Language saved as your favorite!"
msgstr "ilo li awen sona e pona pi toki ni!"

#: core/support.php:16
msgid "Support Pepper&Carrot on:"
msgstr "o pana e mani tawa jan Pepa&soweli Kawa lon ilo ni:"

#: core/support.php:19
msgid "Patreon"
msgstr "ilo Patreon"

#: core/support.php:22
msgid "Other possibilities:"
msgstr "ken ante:"

#: core/support.php:29
msgid "Wire transfer:"
msgstr "nasin pi tomo mani:"

#: core/support.php:35
msgid "Address for gifts:"
msgstr "sina wile pana e ijo la, o pana e ona tawa tomo ni:"

#: core/viewer.php:58
msgid "source and license"
msgstr "pali mama en nasin lawa"

#. Generate the string for the browser's windows in the art slideshow viewer
#. %1$s = artwork title, %2$s = author
#. Image (big)
#. -----------
#: core/viewer.php:64 core/viewer.php:84 index.php:283
#, php-format
msgid "%1$s by %2$s"
msgstr "sitelen %1$s tan jan %2$s"

#. Close button
#: core/viewer.php:69 core/viewer.php:96
msgid "Return to the gallery of thumbnails."
msgstr "o kama sin lon lipu lukin pi sitelen ale."

#: core/viewer.php:115
msgid "Artist:"
msgstr "jan musi:"

#: core/viewer.php:116
msgid "Original size:"
msgstr "suli pi sitelen mama:"

#: core/viewer.php:117
msgid "Created on:"
msgstr "pali ni li tan tenpo ni:"

#: core/viewer.php:118
msgid "Category:"
msgstr "kulupu pi ijo musi:"

#: core/viewer.php:126
msgid "Download the high resolution picture"
msgstr "o kama jo e sitelen suli"

#: core/viewer.php:133
msgid "Download the source file (zip)"
msgstr "o kama jo e sitelen mama (zip)"

#: core/viewer.php:146
msgid "Attribution to"
msgstr "o pana e nimi ni:"

#. fan-art license exception
#: core/viewer.php:150
#, php-format
msgid ""
"This picture is fan-art made by %s. It is reposted on the fan-art gallery of "
"Pepper&Carrot with permission."
msgstr ""
"sitelen ni li pali sama pali mi, li tan jan ni: %s. mi pana e ona lon lipu "
"ilo ni tan ni: jan musi ona li toki e ken."

#: core/viewer.php:151
msgid ""
"Do not reuse this picture for your project unless you obtain author's "
"permissions."
msgstr ""
"jan musi pi sitelen ni li pana ala e ken tawa sina la, o kepeken ala sitelen "
"ni lon pali sina."

#: core/webcomic.php:91
msgid "Display the transcript for this episode."
msgstr "o pana e toki ale tan lipu ni."

#: core/webcomic.php:117
msgid "No transcript available for this episode."
msgstr "lipu ni li ken ala pana e toki ona."

#: core/webcomic.php:137
msgid "Display the pages of this episode in high resolution."
msgstr "lipu ni la, o pana e sitelen suli."

#: core/webcomic.php:137
msgid "High resolution"
msgstr "sitelen suli"

#: core/webcomic.php:138
msgid "Transcript"
msgstr "ale toki"

#: core/webcomic.php:139
msgid ""
"Display the full set of sources files for this episode in this language."
msgstr ""
"lipu ni la, o pana e sitelen mama e lipu mama e toki mama e ijo ante mama."

#: core/webcomic.php:139
msgid "Sources and license"
msgstr "pali mama en nasin lawa"

#: core/webcomic.php:143
msgid ""
"Oops! There is no translation available yet for this episode with the "
"language you selected. The page will continue in English."
msgstr ""
"pakala! lipu ni li lon ala kepeken toki pi wile sina. lipu li awen kepeken "
"toki Inli."

#: core/webcomic.php:213 core/webcomics.php:62
#, php-format
msgid "Read the %d comment on the blog."
msgid_plural "Read the %d comments on the blog."
msgstr[0] "o lukin e toki %d tan jan ante lon anpa pi toki mi."
msgstr[1] "o lukin e toki %d tan jan ante lon anpa pi toki mi."

#: core/webcomic.php:214
#, php-format
msgid "Read %d comment."
msgid_plural "Read %d comments."
msgstr[0] "o lukin e toki %d tan jan ante."
msgstr[1] "o lukin e toki %d tan jan ante."

#: core/webcomics.php:9
msgid "All episodes"
msgstr "lipu musi ale"

#: core/webcomics.php:57
msgid "(click to open the episode)"
msgstr "(sina wile lukin la, o luka e ni)"

#: core/webcomics.php:62
#, php-format
msgid "Published on %1$s, <a %2$s>%3$d comment</a>."
msgid_plural "Published on %1$s, <a %2$s>%3$d comments</a>."
msgstr[0] "lipu ni li kama lon tenpo %1$s, <a %2$s>toki %3$d tan jan ante</a>."
msgstr[1] "lipu ni li kama lon tenpo %1$s, <a %2$s>toki %3$d tan jan ante</a>."

#: index.php:283
#, fuzzy
msgid "Viewer"
msgstr "ilo lukin"

#. In other case, redirect mode to better Gettext translated labels
#: index.php:292
msgid "Philosophy"
msgstr "nasin"

#: index.php:295
msgid "Documentation"
msgstr "sona ilo"

#: index.php:297
#, fuzzy
msgid "Files"
msgstr "ijo ilo"

#: index.php:301 index.php:318
msgid "Fan-art"
msgstr "pali pona pi jan ante"

#: index.php:303
#, fuzzy
msgid "Website setup"
msgstr "open pi pali ilo kama"

#: index.php:305
#, fuzzy
msgid "Support"
msgstr "o pana e pona"

#: index.php:307
msgid "Chat rooms"
msgstr "kulupu toki"

#: index.php:315
msgid "Book-publishing"
msgstr "pali pi lipu kiwen"

#: index.php:316
msgid "Commissions"
msgstr "pali sitelen esun"

#: index.php:319
msgid "Framasoft"
msgstr "ilo Framasoft"

#: index.php:321
msgid "Press"
msgstr "esun sona"

#: index.php:322
msgid "References"
msgstr "sitelen pi pali sama"

#: index.php:324
msgid "Vector"
msgstr "sitelen pi nasin linja"

#: index.php:331
msgid "Not found:"
msgstr "ni li lon ala:"
